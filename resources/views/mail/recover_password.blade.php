<html>
<head>
</head>
<body class="email_recover" style="margin: 0; padding: 0; font-family: Arial; background: #f3f3f3;">

<div class="header" style="background: #ffffff; text-align: center; padding: 15px 0;">
    <div class="container" style="display: block; max-width: 650px; width: 100%; margin: 0 auto;">
        <img src="{{ asset('assets/images/logo_solid.png') }}" style="width: 100%; max-width: 120px;" />
    </div>
</div>

<div class="content" style="width: 100%; display: inline-block; padding: 20px 0;">
    <div class="container" style="display: block; max-width: 650px; width: 100%; margin: 0 auto;">
        <br>
        <p class="title" style="display: inline-block; margin: 0; font-size: 20px;">Restablecimiento de contraseña</p>
        <br>
        <br>
        <br>
        <p class="subtitle" style="display: inline-block; margin: 0; font-size: 17px;"><strong>¡Hola {{ $user->name }}!</strong></p>
        <br>
        <br>
        <p class="subtitle" style="display: inline-block; margin: 0; font-size: 17px;">¿Olvidaste tu contraseña?</p>
        <br>
        <br>
        <p class="subtitle" style="display: inline-block; margin: 0; font-size: 17px;">Recibimos una solicitud para cambiar la contraseña de tu cuenta.</p>
        <br>
        <br>
        <p class="subtitle" style="display: inline-block; margin: 0; font-size: 17px;">Si no has solicitado este cambio, puedes ignorar este correo.</p>
        <br>
        <br>
        <br>
        <div style="text-align: center;">
            <a href="{{ url("change_password") }}/{{ $token }}" style="text-decoration: none; display: inline-block; background: #8cb7d6; color: white; font-size: 15px; padding: 8px 10px; border-radius: 3px;">Cambiar contraseña</a>
        </div>
        <br>
        <br>
        <br>
    </div>
</div>


</body>
</html>