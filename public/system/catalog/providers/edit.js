$(document).ready(function () {

    $(this).on("click", ".add-new-comment", function (e) {

        var area_text = "";
        area_text +=  '<div class="form-group">';
        area_text +=  '     <div class="row">';
        area_text +=  '         <div class="col-md-10">';
        area_text +=  '             <textarea class="form-control" name="nota[]" rows="4"></textarea>';
        area_text +=  '         </div>';
        area_text +=  '         <div class="col-md-2">';
        area_text +=  '             <div class="btn btn-danger" onclick="$(this).parent().parent().parent().remove();"><i class="fas fa-minus-circle"></i> Eliminar</div>';
        area_text +=  '         </div>';
        area_text +=  '     </div>';
        area_text +=  '</div>';

        $("#notas-cliente").append(area_text);

    });



    $(this).on("click", ".add-new-attach", function (e) {

        var area_text = "";
        area_text +=  '<div class="form-group">';
        area_text +=  '     <div class="row">';
        area_text +=  '         <div class="col-md-10">';
        area_text +=  '             <input name="attach[]" class="form-control" type="file">';
        area_text +=  '         </div>';
        area_text +=  '         <div class="col-md-2">';
        area_text +=  '             <div class="btn btn-danger" onclick="$(this).parent().parent().parent().remove();"><i class="fas fa-minus-circle"></i> Eliminar</div>';
        area_text +=  '         </div>';
        area_text +=  '     </div>';
        area_text +=  '</div>';

        $("#adjuntos-cliente").append(area_text);

    });


    $(this).on("click", ".remove-note", function (e) {

        var href = $(this).attr("href");
        var dom_note = $(this);

        Swal.fire({
            title: 'Mensaje',
            text: '¿Deseas eliminar la nota seleccionada?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'get',
                    url: href,
                    dataType: 'json',
                    beforeSend: function () {
                        blockForm();
                    },
                    error: function () {
                        unblockForm();

                        errorAlert('Experimentamos fallas técnicas, intentelo más tarde.');
                    },
                    success: function (response) {
                        unblockForm();

                        if (response.status == true) {
                            dom_note.parent().parent().parent().remove();
                        } else {
                            errorAlert(response.message);
                        }
                    }
                });

            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // Swal.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // )
            }
        })

    });


    $(this).on("click", ".remove-attachment", function (e) {

        var href = $(this).attr("href");
        var dom_note = $(this);

        Swal.fire({
            title: 'Mensaje',
            text: '¿Deseas eliminar el archivo adjunto seleccionado?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'get',
                    url: href,
                    dataType: 'json',
                    beforeSend: function () {
                        blockForm();
                    },
                    error: function () {
                        unblockForm();

                        errorAlert('Experimentamos fallas técnicas, intentelo más tarde.');
                    },
                    success: function (response) {
                        unblockForm();

                        if (response.status == true) {
                            dom_note.parent().parent().parent().parent().parent().parent().remove();
                        } else {
                            errorAlert(response.message);
                        }
                    }
                });

            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // Swal.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // )
            }
        })

    });



});