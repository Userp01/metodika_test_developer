$(document).ready(function () {

    $('#table_principal').DataTable({
        // dom: 'Bfrtip',
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ],
        "aaSorting": [],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No existen registros para mostrar",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No existen registros para mostrar",
            "infoFiltered": "(Filtrando de _MAX_ registros)",
            "paginate": {
                "previous": "Anterior",
                "next": "Siguiente"
            },
            "search": "Buscar"
        }
    });
    //$('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');


    $(this).on('click', '.btn-delete', function (e) {
        e.preventDefault();

        var route = $(this).attr('href');
        var message_used = "";



        Swal.fire({
            title: 'Mensaje',
            text: '¿Deseas eliminar el registro seleccionado?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    type: 'get',
                    url: route,
                    dataType: 'json',
                    beforeSend: function () {
                        blockForm();
                    },
                    error: function () {
                        unblockForm();

                        errorAlert('Experimentamos fallas técnicas, intentelo más tarde.');
                    },
                    success: function (response) {
                        unblockForm();

                        if (response.status == true) {
                            swal({
                                title: "¡Excelente!",
                                text: response.message,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonText: "Aceptar",
                            }).then((result) => {
                                window.location = response.url;
                            });
                        } else {
                            errorAlert(response.message);
                        }
                    }
                });

            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // Swal.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // )
            }
        })

    });


    $(this).on("click", ".view-info", function (e) {
        e.preventDefault();

        var route = $(this).attr("href");
        var title_dialog = $(this).attr("title-dialog");


        $.ajax({
            type: 'get',
            url: route,
            dataType: 'json',
            beforeSend: function () {
                blockForm();
            },
            error: function () {
                unblockForm();

                errorAlert('Experimentamos fallas técnicas, intentelo más tarde.');
            },
            success: function (response) {
                unblockForm();

                swal({
                    width: 500,
                    title: title_dialog,
                    html: response,
                    showCancelButton: false,
                    confirmButtonText: "Aceptar",
                    customClass: 'w-75',
                });

            }
        });

    })


});