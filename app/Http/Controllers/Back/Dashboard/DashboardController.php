<?php

namespace MetodikaTI\Http\Controllers\Back\Dashboard;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use MetodikaTI\AppUser;
use MetodikaTI\Http\Controllers\Controller;
use MetodikaTI\Order;

class DashboardController extends Controller
{

    public function index()
    {

        $app_users = "";
        $orders_terminado = "";
        $orders_pendientes = "";

        return view("back.dashboard.dashboard", ["app_users" => $app_users, "orders_terminado" => $orders_terminado, "orders_pendientes" => $orders_pendientes]);

    }

}
